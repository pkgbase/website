# website

Website for pkgbase.live

## Note

This project is now shelved, as I've run out of sponsor support,
and the FreeBSD project is providing PkgBase builds themselves:

https://lists.freebsd.org/archives/freebsd-pkgbase/2023-October/000221.html
