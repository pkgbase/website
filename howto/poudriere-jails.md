---
title: "Howto: Setting up pkgbase poudriere Jails"
author: Evilham
lang: en
---

Currently [poudriere](https://github.com/freebsd/poudriere/wiki) does not support `pkgbase` jails, so we have to do that ourselves.

This is a condensed and adapted version of ["Setting up Jails"](/howto/jails.html), focused on setting up a jail that can be used by poudriere to build ports.

Hopefully this helps as documentation on how to expand poudriere to support `pkgbase` jails.

## Variable assumptions

Following assumes the variable `JAIL=140Ramd64` is set to your jail name, see `CAVEATS` in [`poudriere-jail(8)`](https://man.freebsd.org/poudriere-jail(8)) for name limitations.
Change `amd64` for your architecture as necessary.

Any other variables correspond to those defined in `/usr/local/etc/poudriere.conf`.

## ZFS setup

Depending on your setup:

```shell=sh
# zfs create -o mountpoint=${BASEFS}/jails/${JAIL} \
    ${ZPOOL}${ZROOTFS}/jails/${JAIL}
```

## Install

On top of the barebones base system as mentioned in ["Setting up Jails"](/howto/jails.html), we need `FreeBSD-locale`, `FreeBSD-clang` and `FreeBSD-lld` to be able to build ports:

```shell=sh
# pkg -r ${BASEFS}/jails/${JAIL} install -r FreeBSD-base FreeBSD-clibs \
    FreeBSD-clibs-dev FreeBSD-libexecinfo FreeBSD-rc \
    FreeBSD-runtime FreeBSD-runtime-dev \
    FreeBSD-utilities FreeBSD-utilities-dev \
    FreeBSD-zoneinfo FreeBSD-locales FreeBSD-clang FreeBSD-lld \
    FreeBSD-libcompiler_rt FreeBSD-libcompiler_rt-dev \
    FreeBSD-libarchive FreeBSD-libarchive-dev \
    FreeBSD-libbz2 FreeBSD-libbz2-dev \
    FreeBSD-liblzma FreeBSD-liblzma-dev FreeBSD-openssl-dev \
    FreeBSD-libbsm-dev
```

## Register the jail in poudriere

We will trick poudriere into loading the newly created jail as follows:

```shell=sh
# export JAILCFG=/usr/local/etc/poudriere.d/jails/${JAIL}
# mkdir ${JAILCFG}
# echo amd64 > ${JAILCFG}/arch
# echo ${ZPOOL}${ZROOTFS}/jails/${JAIL} > ${JAILCFG}/fs
# echo null > ${JAILCFG}/method
# echo ${BASEFS}/jails/${JAIL} > ${JAILCFG}/mnt
# echo /usr/src > ${JAILCFG}/srcpath
# date '+%s' > ${JAILCFG}/timestamp
# echo '14.0-CURRENT' > ${JAILCFG}/version
```

Particularly important is setting `method` to `null` and `srcpath` to `/usr/src`, or to the path containing your OS source code.
This will be necessary, for example to build kernel modules, so take care to have this source code match your OS and kernel version.

For poudriere it is also very important to have a snapshot called `@clean`, which is what it will rollback to after builds:

```shell=sh
# zfs snapshot ${ZPOOL}${ZROOTFS}/jails/${JAIL}@clean
```

## Build ports

Now you should be able to use the jail as usual with:

```shell=sh
# poudriere bulk -j ${JAIL} graphics/drm-current-kmod
```


## Updating the jail

This can be done as usual, but it is important to keep the `@clean` snapshot pointing to the desired state.
Something as follows will do:

```shell=sh
# zfs rollback -r ${ZPOOL}${ZROOTFS}/jails/${JAIL}@clean
# zfs rename ${ZPOOL}${ZROOTFS}/jails/${JAIL}@clean ${ZPOOL}${ZROOTFS}/jails/${JAIL}@clean-old
# pkg -r ${BASEFS}/jails/${JAIL} upgrade -r FreeBSD-base
# zfs snapshot ${ZPOOL}${ZROOTFS}/jails/${JAIL}@clean
```
