---
title: Unofficial FreeBSD pkgbase Howto
author: Mina Galić
lang: en
---

This howto is a focused extract from the [FreeBSD wiki on PkgBase](https://wiki.freebsd.org/PkgBase). Since that wiki does constitute the current documentation on `pkgbase`, I recommend giving it a read.

Due to its size, we'll split this howto into three parts:

- [Bootstrapping an Existing Installation](/howto/bootstrap.html)
- [Performing release upgrades](/howto/abi-changes.html)
- [Setting up Jails](/howto/jails.html)
- [Setting up a Fresh Install](/howto/fresh.html)
- [Setting up pkgbase poudriere Jails](/howto/poudriere-jails.html)

---

## Meta

- [How did she do it?!](/howto/howdo.html)
