---
title: Unofficial FreeBSD pkgbase repository
author: Mina Galić
lang: en
---

Welcome to `alpha.pkgbase.live`, the unofficial repository for the FreeBSD [PkgBase project](https://wiki.freebsd.org/PkgBase).

The package sets below are built with [poudriere(8)](https://github.com/freebsd/poudriere/pull/799).

Add a configuration file for the PkgBase repo:

`/usr/local/etc/pkg/repos/base.conf`

This `base.conf` example is for the 13.2 `release` branch (note the URL):

```
# FreeBSD pkgbase repo

FreeBSD-base: {
  url: "https://alpha.pkgbase.live/release/13.2/${ABI}/latest",
  signature_type: "pubkey",
  pubkey: "/usr/local/etc/pkg/keys/alpha.pkgbase.live.pub",
  enabled: yes
}
```

– if you prefer, specify the `stable` or `current` branch.

Then put in place a copy of [our public key](alpha.pkgbase.live.pub):

```
~ $ sh
# sudo mkdir -p /usr/local/etc/pkg/keys/ && \
      sudo fetch --output=/usr/local/etc/pkg/keys/alpha.pkgbase.live.pub \
      https://alpha.pkgbase.live/alpha.pkgbase.live.pub
# exit
```

## Package Sets

### `13.2-RELEASE`

* [FreeBSD:13:amd64](/release/13.2/FreeBSD:13:amd64/)
* [FreeBSD:13:aarch64](/release/13.2/FreeBSD:13:aarch64/)
* [FreeBSD:13:armv7](/release/13.2/FreeBSD:13:armv7/)
* [FreeBSD:13:i386](/release/13.2/FreeBSD:13:i386/)

### `13-STABLE`

* [FreeBSD:13:amd64](/stable/FreeBSD:13:amd64/)
* [FreeBSD:13:aarch64](/stable/FreeBSD:13:aarch64/)
* [FreeBSD:13:armv7](/stable/FreeBSD:13:armv7/)
* [FreeBSD:13:i386](/stable/FreeBSD:13:i386/)

### `14-CURRENT`

* [FreeBSD:14:amd64](/current/FreeBSD:14:amd64/)
* [FreeBSD:14:aarch64](/current/FreeBSD:14:aarch64/)
* [FreeBSD:14:armv7](/current/FreeBSD:14:armv7/)
* [FreeBSD:14:i386](/current/FreeBSD:14:i386/)

## How To

While the [PkgBase Wiki](https://wiki.freebsd.org/PkgBase) provides some info on how to get started, I would like to distill it, so check out my [Howto](howto/index.html)

## Pending Changes

Rather than doing any awful hacks myself, I like to push changes upstream.
This also feels more in spirit with the Call-for-Testing.

* [HTTP Caching of packages](https://github.com/freebsd/poudriere/pull/751)

## ToDo

* figure out how to configure IPv6 on Vultr
* fix bsdinstall and offer ISO downloads which allows for an easy bootstrap!
* create bootloader (post) install script to update `loader.efi`
* fix FreeBSD-base packages' `origin` to *not only* say `base`, but `FreeBSD-base/foo`.
* fix [FreeBSD-clang dependency](https://bugs.freebsd.org/254173) issue that doesn't install all dependencies needed to actually yield a functional compiler
* fix [wrong package split (rmdma)](https://bugs.freebsd.org/263227). Found when upgrading to 13.1

## Done

* Rotate repo key, because I EXPOSED the PRIVATE KEY as pubkey
* Fix [long stading pkg error](https://codeberg.org/pkgbase/website/issues/3) by moving Pub Key to a different directory
* Now on vultr!
* added i386, dropped riscv64
* added 13.2
* Fixes to various sytems to deal with .pkgsave files left from bootstrapping
  - [rc](https://reviews.freebsd.org/D27962)
  - [kldxref](https://reviews.freebsd.org/D27959)
* Added [FIRECRACKER](https://www.daemonology.net/blog/2022-10-18-FreeBSD-Firecracker.html) [FreeBSD:14:amd64](/current/FreeBSD:14:amd64/) `KERNCONF`.
* [autoremove bug](https://bugs.freebsd.org/254050) fixed! which removes a little too much
* This build server is now self-hosting!
* In addition, we've scaled up this server to a modern vServer using [modern versions of Virtio](https://bugs.freebsd.org/236922#c65)!
* [Package Signing](https://github.com/freebsd/poudriere/issues/812)
* [Building multiple kernel](https://github.com/freebsd/poudriere/pull/819)
* [And packaging multiple Kernels](https://github.com/freebsd/poudriere/pull/823)
* build `WITH_REPRODUCIBLE_BUILD=YES`
* offer "production build" — stable/13 is now branched!
* added MMCCAM kernels to our list of built kernels
* Added arvm7 as platform, because that platform is unsupported by freebsd-update
* Added RISC-V as platform, because that platform is unsupported by freebsd-update
* [fixed bug](https://reviews.freebsd.org/D29224) that makes it [impossible to uninstall -dbg packages](https://bugs.freebsd.org/254174), because they are marked as `vital`.
* The server was migrated to dedicated hardware and runs in a bhyve jail
* poudriere now no longer uses ZFS, this should make future migrations easier
