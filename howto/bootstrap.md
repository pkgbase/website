---
title: "Howto: Bootstrap an existing Install"
author: Mina Galić
lang: en
---

To bootstrap `pkgbase`, we'll use [`bectl(8)`](https://man.freebsd.org/bectl(8)). To be fair, you should use `bectl(8)` whenever you make a major change to a system!

## Create a new Boot Environment

To make our lives easier, we can set up the repository as described on the [front page](https://alpha.pkgbase.live/) *before* creating the BE.

Another way to make our lives easier is to create a `-r`ecursive boot environment:

```
~ $ sudo bectl create -r PkgBase
```

## Setup

To use our BE, we'll enter a jail:

```
~ $ sudo -H bectl jail -o host=inherit \
    -o ip4=inherit -o ip6=inherit \
    -o allow.chflags=1 -o mount.devfs=1 \
    -o devfs_ruleset=4 PkgBase
```

This command will mount the BE in a temporary directory, and drop us into a jail with the `bectl(8)` documented defaults and our `-o`verrides.
From the set above, the network options allow us to use the host's network, and the `allow.chflags` option allows us to modify protected files and directories, which is what we're about to do.

We are now ready to bootstrap:

```
# pkg update -r FreeBSD-base
# pkg install -r FreeBSD-base -g 'FreeBSD-*'
```

– this `-g`lob-matches and installs all packages in the `FreeBSD-base` repo with names that begin with `FreeBSD-`.

Then we need to:

## Cleanup

`alpha.pkgbase.live` builds all sensible kernel packages:

```
~ $ pkg info | grep FreeBSD-kernel
FreeBSD-kernel-generic-13.0.a2.20210123174755 FreeBSD GENERIC kernel
FreeBSD-kernel-generic-dbg-13.0.a2.20210123174755 FreeBSD GENERIC kernel -dbg
FreeBSD-kernel-minimal-13.0.a2.20210123174755 FreeBSD MINIMAL kernel
FreeBSD-kernel-minimal-dbg-13.0.a2.20210123174755 FreeBSD MINIMAL kernel -dbg
```

The `generic` kernel is the default kernel. We can either:

* decide which kernel to keep, and `pkg-remove(8)` all others; or
* leave them all installed and choose with the bootloader.

### .pkgsave

Since `pkg(8)` "knows" that none of the files existing on your system prior to `pkgbase` installation are part of a package, it "saves" them as `.pkgsave` files. Most of those files can be deleted.  Not so these in `/etc`:

```
/etc/aliases
/etc/group
/etc/master.passwd
/etc/ssh/ssh_config
/etc/ssh/sshd_config
/etc/sysctl.conf
/etc/syslog.conf
/etc/ttys
```

– and these root profiles:

```
/root/.cshrc
/root/.k5login
/root/.login
/root/.profile
/root/.shrc
```

In theory, you can simply copy the `.pkgsave` over the base-file.
To ensure that you'll get what you expect, you can `diff(1)` beforehand.

If, like me, you messed up anywhere along the way, the safest way to restore these files is to copy them from the host into the jail. We can look up where the jail is mounted and copy the files from the host in:

```
~ $ bectl list -c creation
BE      Active Mountpoint         Space Created
default NR     /                  19.5G 2020-12-14 10:38
PkgBase -      /tmp/be_mount.UxgO 57.1G 2021-01-20 20:38
```

After restoring `/etc/master.passwd` from the host, it might be tempting to also restore `/etc/spwd.db` and `/etc/pwd.db`, however it is far more reliable to open `vipw(8)` then save-close it.

Unless you knowingly modified any existing file, it is now time to permanently remove all `.pkgsave` files:

```
~ $ sudo find /libexec/ /lib /rescue/ /usr/ /bin /sbin /boot \
        -name \*.pkgsave -delete
```

– and rebuild `/boot/kernel/linker.hints`:

```
~ $ sudo -H service kldxref onerestart
```

You can now `exit` the jail.

## UEFI

If your computer has UEFI, there is one more step before you reboot to fully experience all the new goodies. You must copy the new `loader.efi` to the EFI partition.

First, find it:

```
~ $ geom label status -s
msdosfs/EFISYS  N/A  ada0p1
  gpt/gptboot0  N/A  ada0p2
```

– then mount it:

```
~ $ sudo mount -t msdosfs /dev/msdosfs/EFISYS /boot/efi
```

To save ourselves the trouble of finding this next time, we can add a line to `/etc/fstab`:

```
/dev/msdosfs/EFISYS     /boot/efi       msdos   rw,noauto       0       0
```

Now we can:

```
~ $ sudo cp /boot/loader.efi /boot/efi/EFI/freebsd/loader.efi
```

– and reboot.

## See also

- [Performing release upgrades](/howto/abi-changes.html)
