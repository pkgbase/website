# Adding a new page

When you add a page, remember to add it to the `Makefile` in the corresponding section.

Also, do take the time to review other, pre-existing pages and inter-link them with "See also" sections or complementing the existing prose.

# Style

## Markdown

It is preferred to in-line links (for example: `[home](/)`).
When that is not convenient, it would be best to have them all together at the end of the document (or the end of the section if the document is longer) instead of having them spread out and interfering with the rest of the text.

It is also preferred not to split lines in the middle of a phrase, instead use natural language breaks.

## Abbreviations

To facilitate reading, use the full version of expressions whenever possible.
Meaning: do prefer "for example" over "e.g.", or "that is", over "i.e.", and so on.
