---
title: "Howto: Performing release upgrades"
author: Evilham
lang: en
---

After we [bootstrap `pkgbase`](/howto/bootstrap.html), we may want to change the FreeBSD version we are running.

As before any major change to the system, we'll use [`bectl(8)`](https://man.freebsd.org/bectl(8)) first, check the [how to bootstrap `pkgbase`](/howto/bootstrap.html) page for an in-depth explanation.

If instead of using `"https://alpha.pkgbase.live/release/${ABI}/latest"` as the [`pkg(8)`](https://man.freebsd.org/pkg(8)) repository we hardcode the `ABI` (for example by using `FreeBSD:13:amd64` instead of `${ABI}`), we may get an error like:

```
# pkg update -r FreeBSD-base
Updating FreeBSD-base repository catalogue...
Fetching meta.conf: 100%    163 B   0.2kB/s    00:01
Fetching packagesite.txz: 100%   66 KiB  67.1kB/s    00:01
Processing entries:   0%
pkg: wrong architecture: FreeBSD:13:* instead of FreeBSD:12:amd64
pkg: repository FreeBSD-base contains packages with wrong ABI: FreeBSD:13:*
Processing entries: 100%
Unable to update repository FreeBSD-base
Error updating repositories!
```

## Forcing the `ABI` value

Instead of hardcoding the value for `ABI`, we are better off instructing `pkg(8)` to use a different `ABI` by defining its `ABI` environment variable as follows (this is documented in [`pkg.conf(5)`](https://man.freebsd.org/pkg.conf(5)) under `VARIABLES`):

```
# env ABI=FreeBSD:13:amd64 pkg update -r FreeBSD-base
```

This will enable us to upgrade to a different FreeBSD version.

For example, to upgrade from 12.1-RELEASE to 13.0-RELEASE, we can run:

```
# env ABI=FreeBSD:13:amd64 pkg upgrade -r FreeBSD-base
```

## Switching from RELEASE to CURRENT

Note that if we are switching from a RELEASE version to a CURRENT version, we also have to change the repository's URL to reflect that, that is:

```
# FreeBSD pkgbase repo

FreeBSD-base: {
  url: "https://alpha.pkgbase.live/current/${ABI}/latest",
  signature_type: "pubkey",
  pubkey: "/usr/share/etc/pkg/keys/alpha.pkgbase.live.pub",
  enabled: yes
}
```

Notice the `current` instead of `release` in the URL field.

## Safety net

Remember that with Boot Environments you can safely go back to a RELEASE version of the OS and that Boot Environments are now even selectable from the bootloader.

## Other considerations

This method might support system downgrades, though that's not officially supported!
It would be an interesting thing to test, of course always in a testing machine.

Once `pkgbase` is better tested and supported, it might be worth it to consider what some kind of compatibility with the [`freebsd-update(8)`](https://man.freebsd.org/freebsd-update(8)) command would look like.
